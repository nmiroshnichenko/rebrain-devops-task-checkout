# Rebrain Devops Course Repo

This repo is just for educational purposes

## Content

Description of the content

### Nginx config

There is a default nginx config file [nginx.conf](nginx.conf) here

## Some garbage just for example of markdown features

### Code snippets
```
events {
	worker_connections 768;
	# multi_accept on;
}
```

### Tables

header 1 | header 2
-- | --
content 1 | content 2


## Authors

* **Nikolai Miroshnichenko** - *Initial work* - [Nikolay Miroshnichenko](https://github.com/nmiroshnichenko)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks to Rebrain for the course
* Thanks to [Linus Torvalds](https://github.com/torvalds) for the Git
* Thanks to the guys from [Github](https://github.com/) for it
